package net.bytesly.ugquiz_broadcastreceiver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    TimeTickReceiver timeTickReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        timeTickReceiver = new TimeTickReceiver();
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter timeFilter = new IntentFilter(Intent.ACTION_TIME_TICK);
        registerReceiver(timeTickReceiver, timeFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(timeTickReceiver);
    }
}